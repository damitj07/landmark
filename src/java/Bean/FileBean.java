package Bean;

public class FileBean {
	String path,des,sub;
	int id;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getId() {
		return id;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	@Override
	public String toString() {
		return "FileBean [path=" + path + ", id=" + id + "]";
	}

	public void setId(int id) {
		this.id = id;
	}

}

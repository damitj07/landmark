package Bean;

public class NewsBean {
	String addHeading;
	String  addBody;
	int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getaddHeading() {
		return addHeading;
	}
	public void setaddHeading(String addHeading) {
		this.addHeading = addHeading;
	}
	public String getaddBody() {
		return addBody;
	}
	public void setaddBody(String addBody) {
		this.addBody = addBody;
	}
	@Override
	public String toString() {
		return "NewsBean [id=" + id + ", addHeading=" + addHeading
				+ ", addBody=" + addBody + "]";
	}
	
	}
	



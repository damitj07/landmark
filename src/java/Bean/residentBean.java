package Bean;

public class residentBean {
	String projectname,description,feature,projecttype,feedback,work,path;
	int projecttypeid,projectid;
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getFeature() {
		return feature;
	}
	public void setFeature(String feature) {
		this.feature = feature;
	}
	public String getProjecttype() {
		return projecttype;
	}
	public String getWork() {
		return work;
	}
	public void setWork(String work) {
		this.work = work;
	}
	public void setProjecttype(String projecttype) {
		this.projecttype = projecttype;
	}
	public String getFeedback() {
		return feedback;
	}
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	public int getProjecttypeid() {
		return projecttypeid;
	}
	public void setProjecttypeid(int projecttypeid) {
		this.projecttypeid = projecttypeid;
	}
	public int getProjectid() {
		return projectid;
	}
	public void setProjectid(int projectid) {
		this.projectid = projectid;
	}
	@Override
	public String toString() {
		return "residentBean [projectname=" + projectname + ", description="
				+ description + ", feature=" + feature + ", projecttype="
				+ projecttype + ", feedback=" + feedback + ", projecttypeid="
				+ projecttypeid + ", projectid=" + projectid + "]";
	}

}

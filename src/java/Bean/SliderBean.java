package Bean;

public class SliderBean {
String path,type;
boolean active;
int id;

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public boolean isActive() {
	return active;
}

public void setActive(boolean active) {
	this.active = active;
}

public String getPath() {
	return path;
}

public void setPath(String path) {
	this.path = path;
}

public String getType() {
	return type;
}

public void setType(String type) {
	this.type = type;
}

}

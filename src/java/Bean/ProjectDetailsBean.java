package Bean;

public class ProjectDetailsBean {
	
	int id;
	String filePath;
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public int getId() {
		return id;
	}
	@Override
	public String toString() {
		return "ProjectDetailsBean [id=" + id + ", projectTypeName="
				+ projectTypeName + ", projectName=" + projectName
				+ ", projectDescription=" + projectDescription + ", features="
				+ features + ", feedback=" + feedback + ", work=" + work + "]";
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProjectTypeName() {
		return projectTypeName;
	}
	public void setProjectTypeName(String projectTypeName) {
		this.projectTypeName = projectTypeName;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectDescription() {
		return projectDescription;
	}
	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}
	public String getFeatures() {
		return features;
	}
	public void setFeatures(String features) {
		this.features = features;
	}
	public String getFeedback() {
		return feedback;
	}
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	public String getWork() {
		return work;
	}
	public void setWork(String work) {
		this.work = work;
	}
	String projectTypeName;
	String projectName;
	String projectDescription;
	String features;
	String feedback;
	String work;

}

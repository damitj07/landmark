package Controller;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.Downloadfile;

/**
 * Servlet implementation class FileUploadHandler1
 */
@WebServlet("/download")
public class FileUploadHandler1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String LIST_USER ="/uploadDocs.jsp";
	 Downloadfile df=new Downloadfile();
	 private static final int BUFFER_SIZE = 4096;
	 private final String UPLOAD_DIRECTORY = "D:/school/ShreeEnterPrises/WebContent/images";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileUploadHandler1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		String forward="";
		System.out.println("hello");
		
		//System.out.println("ses.getAttribute"+request.getParameter("userid1"));
	HttpSession ses=request.getSession();
	
	int uid=Integer.parseInt(request.getParameter("userid").toString());
    System.out.println("userid is Action"+action+ "  "+uid);   
    if(action.equals("download")){
    	String filePath =df.Downlode(uid);
    	System.out.println("before replace"+filePath+"  separator"+File.separator);
    	String str=File.separator;
    	String str1="/";
    	String fname="";
    	int start =filePath.indexOf('/');
    	if (start >= 0) {
            fname = filePath.substring(start+1, filePath.length());
            
        }
    	
    	//replaceAll(str,str1);
    	System.out.println("after replace"+fname);
    	ServletContext context = request.getServletContext();
		String appPath = context.getRealPath("/img");
		System.out.println("appPath = " + fname);

		// construct the complete absolute path of the file
		String fullPath = appPath+File.separator + fname;	
		System.out.println("fullpath"+fullPath);
		File downloadFile = new File(fullPath);
		System.out.println("down load path path"+downloadFile);
		FileInputStream inputStream = new FileInputStream(downloadFile);
		
		// get MIME type of the file
		String mimeType = context.getMimeType(fullPath);
		if (mimeType == null) {
			// set to binary type if MIME mapping not found
			mimeType = "application/octet-stream";
		}
		System.out.println("MIME type: " + mimeType);

		// set content attributes for the response
		response.setContentType(mimeType);
		response.setContentLength((int) downloadFile.length());

		// set headers for the response
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"",
				downloadFile.getName());
		response.setHeader(headerKey, headerValue);

		// get output stream of the response
		OutputStream outStream = response.getOutputStream();

		byte[] buffer = new byte[BUFFER_SIZE];
		int bytesRead = -1;

		// write bytes read from the input stream into the output stream
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outStream.write(buffer, 0, bytesRead);
		}

		inputStream.close();
		outStream.close();
    }else if(action.equals("delete")){
    	df.Deleterecords(uid);
    }
			forward = LIST_USER;
			RequestDispatcher rd=request.getRequestDispatcher(LIST_USER);
			request.setAttribute("users", df.getAllArea());
			 rd.forward(request, response);			
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

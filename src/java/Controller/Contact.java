package Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Bean.ContactBean;

/**
 * Servlet implementation class Contact
 */
@WebServlet("/Contact")
public class Contact extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Contact() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	ContactBean cbean=new ContactBean();
	cbean.setName(request.getParameter("userName"));
	cbean.setMobno(request.getParameter("userPhone"));
	cbean.setEmail(request.getParameter("userEmail"));
	cbean.setDes(request.getParameter("userMsg"));
	Contactdao cdao=new Contactdao();
	String action=request.getParameter("action");
	if(action.equalsIgnoreCase("insert")){
		cdao.Addrecords(cbean);
	}else{
		
	}
	
	
	
	

    RequestDispatcher view=request.getRequestDispatcher("contactmap.jsp");
	 
      view.forward(request, response);
	
	}

}

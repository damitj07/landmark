package Controller;
import java.io.File;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import Dao.Downloadfile;





import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
maxFileSize=1024*1024*10,      // 10MB
maxRequestSize=1024*1024*50)
@WebServlet("/upload")
	public class FileUploadHandler extends HttpServlet {
	    /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private Connection con=null;
	   private static String LIST_USER ="/uploadDocs.jsp"; 
	   private static String display ="/Downloads.jsp"; 
	   Downloadfile df=new Downloadfile();
	   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			String forward="";
			String action = request.getParameter("action");
		HttpSession ses=request.getSession();
		
	            if (action.equalsIgnoreCase("listUser")) {			
				forward = LIST_USER;
				
				
			}
	            else if (action.equalsIgnoreCase("display")) {
					System.out.println(" delete Block In Controler");
				
					
					forward = display;
					

				} 
				RequestDispatcher view=request.getRequestDispatcher(forward);
				request.setAttribute("users",df.getAllArea());
			      view.forward(request, response);
					// TODO Auto-generated method stub
				
//		   RequestDispatcher rd=request.getRequestDispatcher(LIST_USER);
//		   	 request.setAttribute("users",df.getAllArea());
//		   	        rd.forward(request, response);
		}
	    @Override
	     protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	    	con=Utility.DbUtil.getConnection();
	        //process only if its multipart content
	    
	    	ServletContext context = request.getServletContext();
			String appPath = context.getRealPath("/img");
			
			File fileSaveDir=new File(appPath);
			System.out.println("fileSaveDir"+fileSaveDir);
            if(!fileSaveDir.exists()){
                fileSaveDir.mkdir();
            }
        String str="/",sub="",des="";
        if(request.getParameter("subject").equals("")){
        	
        }else{
        	sub=request.getParameter("subject");
        }
        if(request.getParameter("productDetails").equals("")){
        	
        }else{
        	des=request.getParameter("productDetails");
        }
        Part part=request.getPart("file");
        
        String fileName=extractFileName(part);
        part.write(appPath + File.separator + fileName);
        String filePath= "images" + str + fileName;
        
       try{
        PreparedStatement pst=(PreparedStatement) con.prepareStatement("insert into files(filepath,filesubject, description )values('"+filePath+"','"+sub+"','"+des+"')");
        	
        pst.executeUpdate();
       }catch(Exception e){
    	   
       }
//			if(ServletFileUpload.isMultipartContent(request)){
//	            try {
//	            	System.out.println("hello am here1");
//	                List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
//	                String name="";
//	                
//	                for(FileItem item : multiparts)
//	                {
//	                    if(!item.isFormField())
//	                    {
//	                         name = new File(item.getName()).getName();
//	                        item.write( new File(appPath + File.separator+ name));
//	                       
//	                     }
//	                }
////	                    String savePath = getServletContext().getRealPath("/images");
////	                    
////	                    File fileSaveDir=new File(savePath);
////	                    if(!fileSaveDir.exists()){
////	                        fileSaveDir.mkdir();
////	                    }
////	                    
////	                Part part=request.getPart("file");
////	               
////	                String fileName=extractFileName(part);
////	                part.write(savePath + File.separator + fileName);
////	                System.out.println("hello "+savePath+"part is "+part);
////	                    System.out.println(name+"   UPLOAD_DIRECTORY "+"/"+appPath);
//	                    String path=appPath +File.separator+name;
//	                    
//	                    System.out.println("path is"+path);
//	                    PreparedStatement pst=(PreparedStatement) con.prepareStatement("insert into files(filepath )values('"+path+"')");
//	                    System.out.println("hello" +pst);
//	                    pst.executeUpdate();
//	                    
//	                //}
//	            
//	               //File uploaded successfully
//	               request.setAttribute("message", "File Uploaded Successfully");
//	            } catch (Exception ex) {
//	               request.setAttribute("message", "File Upload Failed due to " + ex);
//	            }         
//	          
//	        }else{
//	            request.setAttribute("message",
//	                                 "Sorry this Servlet only handles file upload request");
//	        }
	        RequestDispatcher rd=request.getRequestDispatcher(LIST_USER);
	   	 request.setAttribute("users",df.getAllArea());
	   	        rd.forward(request, response);
	       // request.getRequestDispatcher("/Result.jsp").forward(request, response);
	      
	    }
	    private String extractFileName(Part part) {
	    	
	    	System.out.println("hello am here at part"+part);
	        String contentDisp = part.getHeader("content-disposition");
	        
	        String[] items = contentDisp.split(";");
	        
	        for (String s : items) {
	            if (s.trim().startsWith("filename")) {
	                return s.substring(s.indexOf("=") + 2, s.length()-1);
	            }
	        }
	        return "";
	    }
	}
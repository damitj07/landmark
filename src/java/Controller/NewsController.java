package Controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Dao.NewsDao;



/**
 * Servlet implementation class NewsController
 */
@WebServlet("/NewsController")
public class NewsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String INSERT_OR_EDIT = "/NewNews.jsp";
	private static String LIST_NEWS = "/NewNews.jsp";
	private static String display = "/aboutus.jsp";
	private static String error = "/Errorpage.jsp";
	private NewsDao dao;  
    /**
     * @throws SQLException 
     * @see HttpServlet#HttpServlet()
     */
    public NewsController() {
        super();
        dao=new NewsDao();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String forward="";
		String action = request.getParameter("action");
		//HttpSession ses=request.getSession();
		int uid=1;//Integer.parseInt(ses.getAttribute("UserID").toString());
		if (action==null) {			
			//String LIST_USER = null;
			forward = LIST_NEWS;
			request.setAttribute("news", dao.getAllNews("all"));
		  	
		} else
		if (action.equalsIgnoreCase("LIST_News")) {			
			//String LIST_USER = null;
			forward = LIST_NEWS;
			request.setAttribute("news", dao.getAllNews("all"));
		  	
		} else if (action.equalsIgnoreCase("delete")) {
			
			int id = Integer.parseInt(request.getParameter("Id"));
			System.out.println(id);
			dao.newsDelete(id);
			forward = LIST_NEWS;
			request.setAttribute("news", dao.getAllNews("List"));

		} else if (action.equalsIgnoreCase("edit")) {
			forward = INSERT_OR_EDIT;
			int id = uid;
			  dao.newsDelete(id);
			request.setAttribute("news",dao.getAllNews("edit"));
		
		} else if(action.equalsIgnoreCase("display")) {
			forward = display;
			request.setAttribute("news", dao.getAllNews("display"));
			
		}else{
			forward = INSERT_OR_EDIT;
			
		}

			RequestDispatcher view=request.getRequestDispatcher(forward);
			 
	      view.forward(request, response);
			// TODO Auto-generated method stub
		}

		/**
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// TODO Auto-generated method stub
			System.out.println("in do post");
			response.setContentType("text/html");
			 Bean.NewsBean news= new Bean.NewsBean();
			// HttpSession session = request.getSession();
				int uid=1;//Integer.parseInt(session.getAttribute("userID").toString());
			 if(request.getParameter("ADD")==null)
			 {

				news.setId(Integer.parseInt(request.getParameter("id")));
				 news.setaddHeading(request.getParameter("Heading"));
				 news.setaddBody(request.getParameter("Body"));
				 dao.newsUpdate(news);
				 //System.out.println("here");
			 }
			 else if(request.getParameter("UPDATE")==null)
			 {
				
				 news.setId(uid);
				 news.setaddHeading(request.getParameter("Heading"));
				 news.setaddBody(request.getParameter("Body"));
				 dao.newsInsert(news);
				 System.out.println("here");
			 }
			 
			 
			 RequestDispatcher view=request.getRequestDispatcher(LIST_NEWS);
			 request.setAttribute("news", dao.getAllNews("All"));
				view.forward(request, response);
				System.out.println("in thishere");
			}

	}

		// TODO Auto-generated method stub
		
	

package Controller;

import java.io.File;
import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.Part;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;

import Dao.ProjectDetailsDao;

/**
 * Servlet implementation class ProjectDetailsController
 */
@WebServlet("/ProjectDetailsController")
public class ProjectDetailsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ProjectDetailsDao pddao=null;
	private static String LIST_PROJECTS = "/addProjectDetails.jsp";  
	String forword="";
    /**
     * private static final long serialVersionUID = 1L;
	private String fileSavePath;
     * @see HttpServlet#HttpServlet()
     */
	
	private String fileSavePath;
	 private static final String UPLOAD_DIRECTORY = "Upload";
    public ProjectDetailsController() {
        super();
        pddao=new ProjectDetailsDao();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
    public void init() {
        fileSavePath = getServletContext().getRealPath("/") + File.separator + UPLOAD_DIRECTORY;/*save uploaded files to a 'Upload' directory in the web app*/
        if (!(new File(fileSavePath)).exists()) {
            (new File(fileSavePath)).mkdir();    // creates the directory if it does not exist        
        }
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action=request.getParameter("action");
		HttpSession ses=request.getSession();
		
		if(action.equalsIgnoreCase("listprojects"))
		{
			forword=LIST_PROJECTS;
			pddao.getAllProjects();
		} else if (action.equalsIgnoreCase("delete"))
		{
			int projectId=Integer.parseInt(request.getParameter("projectId"));
			pddao.deleteProject(projectId);
			forword=LIST_PROJECTS;
			 request.setAttribute("projects", pddao.getAllProjects());

		}
		RequestDispatcher view=request.getRequestDispatcher(forword);
		 request.setAttribute("projects", pddao.getAllProjects());
			view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		Bean.ProjectDetailsBean project=new Bean.ProjectDetailsBean();
		String projectTypeName=request.getParameter("projectType");
		project.setProjectTypeName(request.getParameter("projectType"));
		project.setProjectName(request.getParameter("projectName"));
		project.setProjectDescription(request.getParameter("description"));
		project.setFeatures(request.getParameter("features"));
		project.setFeedback(request.getParameter("feedback"));
		project.setWork(request.getParameter("work"));
		 System.out.println("file is as fllwos"+project);
        System.out.println("request.getParamete"+request.getParameter("ADD"));
		System.out.println("Project name is"+projectTypeName);
//		//multiple files
//				String resp = "";
//		        int i = 1;
//		        resp += "<br>Here is information about uploaded files.<br>";
//		        try {
//		        
//		            MultipartParser parser = new MultipartParser(request, 1024 * 1024 * 1024);  /* file limit size of 1GB*/
//		            Part _part = null;
//		            System.out.println("file is as fllwos1"+_part);
//		            while ((_part = parser.readNextPart()) != null) {
//		            	  System.out.println("file is as fllwos2"+parser.readNextPart());
//		                if (_part.isFile()) {
//		                    FilePart fPart = (FilePart) _part;  // get some info about the file
//		                    String name = fPart.getFileName();
//		                    System.out.println(name+"file is as fllwos3"+fileSavePath);
//		                    String str="img"+File.separator+name;
//		                    if (name != null) {
//		                    	pddao.Imagepath(str,project.getProjectTypeName());
//		                    	System.out.println(name+"file is as fllwos4"+fileSavePath);
//		                        long fileSize = fPart.writeTo(new File(fileSavePath));
//		                        resp += i++ + ". " + fPart.getFilePath() + "[" + fileSize / 1024 + " KB]<br>";
//		                    } else {
//		                        resp = "<br>The user did not upload a file for this part.";
//		                    }
//		                }
//		            }// end while 
//		        } catch (java.io.IOException ioe) {
//		            resp = ioe.getMessage();
//		        } //end file reading
//		
//		
//		
//		
		/*ServletContext context=request.getServletContext();
		String appPath=context.getRealPath("/img");
		
		File fileSaveDir=new File(appPath);
		if(!fileSaveDir.exists()){
			fileSaveDir.mkdir();
		}
		String str="/";
		Part part=request.getPart("file");
		System.out.println("filename"+request.getPart("file"));
		String filename=extractFileName(part);
		System.out.println("filename"+filename);
		part.write(appPath + File.separator + filename);
		String filePath="img"+str+filename;
		project.setFilePath(filePath);
		 */
		        
				
        if(request.getParameter("UPDATE")==null)
		 {
			 project.setId(Integer.parseInt(request.getParameter("projectid")));
			 int projectId=Integer.parseInt(request.getParameter("projectid"));
			 pddao.updateProject(project);
		
		 }	 else if(request.getParameter("ADD")==null)
		 {
			 
				pddao.addProjectDetails(project);
		 }
		
		RequestDispatcher view=request.getRequestDispatcher(LIST_PROJECTS);
		 request.setAttribute("projects", pddao.getAllProjects());
			view.forward(request, response);
		
	}
//	 private String extractFileName(Part part) {
//	    	
//	    	System.out.println("part"+part);
//	        String contentDisp = part.getHeader("content-disposition");
//	        
//	        String[] items = contentDisp.split(";");
//	        
//	        for (String s : items) {
//	            if (s.trim().startsWith("filename")) {
//	                return s.substring(s.indexOf("=") + 2, s.length()-1);
//	            }
//	        }
//	        return "";
//	    }

}

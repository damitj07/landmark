package Controller;


import java.io.IOException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Bean.LoinBean;
import Dao.Logindao;

@WebServlet("/login")
public class Logincontroller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logindao dao;
	private static String LoginPage = "/adminLogin.jsp?error=wrong";
	
	private static String HomePage = "/adminPanel.jsp";
	
	private String forward;
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	
	public Logincontroller() {
		super();
		dao = new Logindao();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
//		String action = request.getParameter("action");
//		if(action.equalsIgnoreCase("listUser")) {
//			forward = user1;
//		}
//		RequestDispatcher view = request.getRequestDispatcher(forward);
//		view.forward(request, response);r
		RequestDispatcher view = request.getRequestDispatcher(HomePage);
		view.forward(request, response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		LoinBean login = new LoinBean();
		login.setEmailid(request.getParameter("userid"));
		login.setPassword(request.getParameter("password"));
		
		String name = dao.uservalidation(login);
		String role=null;
		String uid="";
		if(name==null)
		{
			name="";
		}
		if(name.equalsIgnoreCase(request.getParameter("userid"))) {
			forward = HomePage;
			//role=dao.getRole(login);
			uid=dao.getUserId(login);
			//login.setUserRole(role);
			session.setAttribute("userIDKey",login.getEmailid());
			
		} else {
			forward = LoginPage;
		}
		
		
		
		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
	}
}

package Utility;

import java.io.IOException;
import java.io.InputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


import com.mysql.jdbc.Connection;

public class DbUtil {
	private static Connection conn = null;
	public static Connection getConnection() {
		if (conn != null) {
			return conn;

		}else
		{
			Properties prop=new Properties();
			InputStream inputstream=DbUtil.class.getClassLoader().getResourceAsStream("/db.properties");
			
			try {
				prop.load(inputstream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String driver=prop.getProperty("driver");
			String url=prop.getProperty("url");
			String user=prop.getProperty("user");
			String password=prop.getProperty("password");
			try {
				Class.forName(driver);
				try {
					conn=(Connection) DriverManager.getConnection(url, user, password);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return conn;
	}

}

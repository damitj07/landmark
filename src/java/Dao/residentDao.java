package Dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import java.sql.*;

import Bean.residentBean;

public class residentDao {

	PreparedStatement pst = null;
	ResultSet rs = null;
	Connection con = null;

	public residentDao() {
		System.out.println("hello");
		con = Utility.DbUtil.getConnection();
		
	}

	public List<residentBean> getAllArea(String pname) {
		List<residentBean> locations = new ArrayList<residentBean>();
		try {
			if(pname==null){
			pst = con.prepareStatement("SELECT projectname,path FROM projectdetails pd,projectimages pi where pd.pdid=pi.pdid");
			}else{
				pst = con.prepareStatement("SELECT projectname,path FROM projectdetails pd,projectimages pi where pi.pdid in(select pdid from projectdetails where projectname='"+pname+"' ) and pi.pdid=pd.pdid");
			}
			System.out.println("pst is"+pst);
			int i=0;
			rs = pst.executeQuery();
			while (rs.next()) {
				i++;
				residentBean location = new residentBean();
				location.setProjectname(rs.getString(1));
				location.setProjecttype(rs.getString(2));
				locations.add(location);
				System.out.println(locations.get(0));
				location.getProjectname().indexOf(0);
				String str=rs.getString(1);
				
			}
			pst.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return locations;

	}

	public List<residentBean> getAllRecords(String pname) {
		List<residentBean> locations = new ArrayList<residentBean>();
		try {
			pst = con
					.prepareStatement("SELECT  p.pdid,projectname, description, features, feedback, work,path,projecttypename FROM projectdetails p ,projectimages s,projecttype t where t.id=p.ptid and p.ptid in(SELECT id FROM projecttype where projecttypename='"+pname+"') and p.pdid=s.pdid");
			System.out.println("query is"+pst);
			rs = pst.executeQuery();
			while (rs.next()) {
				residentBean location = new residentBean();
				location.setProjectid(rs.getInt(1));
				location.setProjectname(rs.getString(8));
				location.setDescription(rs.getString(3));
				location.setFeature(rs.getString(4));
				location.setFeedback(rs.getString(5));
				location.setWork(rs.getString(6));
				location.setPath(rs.getString(7));
				location.setProjecttype(rs.getString(2));
				locations.add(location);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return locations;
	}

	public int getprojectid(String pname) {
		int id=0;
		con = Utility.DbUtil.getConnection();
		try {
			PreparedStatement pst1=null;
			System.out.println("pst is pst "+pname+con);
			pst1=con.prepareStatement("SELECT s.pdid FROM projectdetails s where projectname='"+pname+"'");
			System.out.println("pst is pst "+pst1);
			ResultSet rs=pst1.executeQuery();
			while(rs.next()){
				id=rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return id;
		
		
	}

	public List<residentBean> getprojectimages(int id, String pname) {
		List<residentBean> locations = new ArrayList<residentBean>();
		try {
			pst = con.prepareStatement("SELECT path,ps.projectname,description FROM projectimages p,projectdetails ps where p.pdid in (SELECT s.pdid FROM projectdetails s where projectname='"+pname+"') and ps.pdid=p.pdid");
			System.out.println("query is"+pst);
			rs = pst.executeQuery();
			while (rs.next()) {
				residentBean location = new residentBean();
				
				location.setProjectname(rs.getString(2));
				location.setDescription(rs.getString(3));
				
				location.setPath(rs.getString(1));
				;
				locations.add(location);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return locations;
	}
}

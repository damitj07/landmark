package Dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;



import Bean.ProjectDetailsBean;
import Utility.DbUtil;

public class ProjectDetailsDao {
	
	Connection con=null;
	private PreparedStatement pst=null;
	private ResultSet rs=null;
	ProjectDetailsBean project=null;
	public ProjectDetailsDao()
	{
		con=DbUtil.getConnection();
		project=new ProjectDetailsBean();
	}

	public void addProjectDetails(ProjectDetailsBean project) {

		
		try{
			
			pst=(PreparedStatement) con.prepareStatement("insert into projectdetails(ptid,projectname, description, features, feedback, work) values ("+getProductTypeid(project.getProjectTypeName())+",'"+project.getProjectName()+"', '"+project.getProjectDescription()+"', '"+project.getFeatures()+"', '"+project.getFeedback()+"', '"+project.getWork()+"')");
			//insert into projectdetails(ptid,projectname, description, features, feedback, work) values (?,?,?,?,?,?)"); 
					pst.executeUpdate();
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	public int getProductTypeid(String productTypename)
	{	int productTypeId=0;
		try{
			pst=(PreparedStatement) con.prepareStatement("SELECT id FROM projecttype where projecttypename='"+productTypename+"' ");
			
			rs=pst.executeQuery();
			if(rs.next())
				productTypeId=rs.getInt(1);
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
		return productTypeId;
	}

	public List<ProjectDetailsBean> getAllProjects() {
		List<ProjectDetailsBean> allprojects=new ArrayList<ProjectDetailsBean>();
		try
		{
			pst=(PreparedStatement) con.prepareStatement("SELECT pd.pdid, p.projecttypename, pd.projectname, pd.description, pd.features, pd.feedback, pd.work FROM projectdetails pd,projecttype p where pd.ptid=p.id");
			rs=pst.executeQuery();
			while(rs.next()){
				ProjectDetailsBean project1=new ProjectDetailsBean(); 
				project1.setId((rs.getInt(1)));
				project1.setProjectTypeName(rs.getString(2));
				project1.setProjectName(rs.getString(3));
				project1.setProjectDescription(rs.getString(4));
				project1.setFeatures(rs.getString(5));
				project1.setFeedback(rs.getString(6));
				project1.setWork(rs.getString(7));
				allprojects.add(project1);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return allprojects;
	}

	public void deleteProject(int projectId) {
		
try {
			
			pst=(PreparedStatement) con.prepareStatement("delete FROM projectdetails where pdid=?");
			pst.setInt(1, projectId);
			System.out.println("pst"+pst);
			pst.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void updateProject(ProjectDetailsBean project) {
try {
			
			pst=(PreparedStatement) con.prepareStatement("update projectdetails set ptid="+getProductTypeid(project.getProjectTypeName())+",projectname='"+project.getProjectName()+"', description='"+project.getProjectDescription()+"', features='"+project.getFeatures()+"', feedback='"+project.getFeedback()+"', work='"+project.getWork()+"' where pdid="+project.getId()+"");
			pst.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void Imagepath(String fileSavePath, String projectTypeName) {
		try {
			PreparedStatement pst1=con.prepareStatement("insert into projectimages( path, pdid) values('"+fileSavePath+"','"+getProductTypeid(projectTypeName)+"')");
			System.out.println(pst1);
			pst1.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
		
		
	}



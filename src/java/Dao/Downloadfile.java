package Dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import Bean.FileBean;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
 
//import org.apache.commons.net.ftp.FTP;
//import org.apache.commons.net.ftp.FTPClient;

public class Downloadfile {
Connection con=null;
	public Downloadfile(){
		con=Utility.DbUtil.getConnection();
	}
public String Downlode(int id){
	String path=null;
	try {
		PreparedStatement pst=(PreparedStatement) con.prepareStatement("SELECT filepath FROM files f where f.id='"+id+"'");
		ResultSet rs=pst.executeQuery();
		while(rs.next()){
			path=rs.getString(1);
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
//	String server = "localhost";
//    int port = 8080;
//    String user = "";
//    String pass = "";
//
//    FTPClient ftpClient = new FTPClient();
//    try {
//
//        ftpClient.connect(server, port);
//        ftpClient.login(user, pass);
//       ftpClient.enterLocalPassiveMode();
//       ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
//
//        // APPROACH #1: using retrieveFile(String, OutputStream)
////        String remoteFile1 = path;
////        File downloadFile1 = new File("D:/"+"path");
////        OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile1));
////        boolean success = ftpClient.retrieveFile(remoteFile1, outputStream1);
////        outputStream1.close();
////
////        if (success) {
////            System.out.println("File #1 has been downloaded successfully.");
////        }
//
//        // APPROACH #2: using InputStream retrieveFileStream(String)
//        String remoteFile2 = path;
//        File downloadFile2 = new File("D:"+"path");
//        System.out.println("Down Load path is"+downloadFile2+"hello "+remoteFile2);
//        OutputStream outputStream2 = new BufferedOutputStream(new FileOutputStream(downloadFile2));
//        InputStream inputStream = ftpClient.retrieveFileStream(remoteFile2);
//        byte[] bytesArray = new byte[4096];
//        int bytesRead = -1;
//        while ((bytesRead = inputStream.read(bytesArray)) != -1) {
//            outputStream2.write(bytesArray, 0, bytesRead);
//        }
//
//        boolean success = ftpClient.completePendingCommand();
//        if (success) {
//            System.out.println("File #2 has been downloaded successfully.");
//        }
//        outputStream2.close();
//        inputStream.close();
//
//    } catch (IOException ex) {
//        System.out.println("Error: " + ex.getMessage());
//        ex.printStackTrace();
//    } finally {
//        try {
//            if (ftpClient.isConnected()) {
//                ftpClient.logout();
//                ftpClient.disconnect();
//            }
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
//    }
    return path;
}

	public List<FileBean> getAllArea(){
    	List<FileBean> locations =new ArrayList<FileBean>();
    	try {
    		String str="";
        PreparedStatement pst=(PreparedStatement) con.prepareStatement("SELECT * FROM landmark.files;");
    			 ResultSet rs=pst.executeQuery();
    			 while(rs.next()){
    				 FileBean location=new FileBean();
    			 location.setId(rs.getInt(1));
    			 str=rs.getString(2);
    			 int i=str.indexOf(".pdf");
    			 int j=str.indexOf(".ppt");
    			 int k=str.indexOf(".xlsx");
    			 if(i>0){
    				 location.setPath("img/pdf.jpg");
    			 }else if(j>0){
    				 location.setPath("img/index.jpg");
    			 }else if(k>0){
    				 
    				 location.setPath("img/images.jpg");
    			 }else{
    			 location.setPath(rs.getString(2));
    			 }
    			 location.setSub(rs.getString(3));
    			 location.setDes(rs.getString(4));
    			 
    			 locations.add(location);
    			 
    			 }
    		} catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	
    		return locations;
    	
           }
	public void Deleterecords(int id) {
		try {
	        PreparedStatement pst=(PreparedStatement) con.prepareStatement("delete FROM files where id='"+id+"'");
	    			 pst.executeUpdate();
	    			 
	    		} catch (SQLException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		}
	    	
		
	}
	public List<FileBean> getAllAreaSlider() {
		List<FileBean> locations =new ArrayList<FileBean>();
    	try {
    		String str="";
        PreparedStatement pst=(PreparedStatement) con.prepareStatement("SELECT * FROM shreewood.slider f;");
    			 ResultSet rs=pst.executeQuery();
    			 while(rs.next()){
    				 FileBean location=new FileBean();
    			 location.setId(rs.getInt(1));
    			 str=rs.getString(2);
    			 int i=str.indexOf(".pdf");
    			 int j=str.indexOf(".ppt");
    			 int k=str.indexOf(".xlsx");
    			 if(i>0){
    				 location.setPath("img/index.jpg");
    			 }else if(j>0){
    				 location.setPath("img/ppt.jpg");
    			 }else if(k>0){
    				 
    				 location.setPath("img/images.jpg");
    			 }else{
    			 location.setPath(rs.getString(2));
    			 }
    			 
    			 
    			 locations.add(location);
    			 
    			 }
    		} catch (SQLException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	
    		return locations;
    	
	}
	public void Deleterecordsslider(int uid) {
		try {
	        PreparedStatement pst=(PreparedStatement) con.prepareStatement("delete FROM shreewood.slider where id='"+uid+"'");
	    			 pst.executeUpdate();
	    			 
	    		} catch (SQLException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	    		}
		
	}
	
	
}

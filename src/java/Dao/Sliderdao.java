package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Utility.DbUtil;

import Bean.ProjectDetailsBean;
import Bean.SliderBean;

public class Sliderdao {
	Connection con=null;
	private PreparedStatement pst=null;
	private ResultSet rs=null;
	
	public Sliderdao()
	{
		con=DbUtil.getConnection();
	
	}
	public List<SliderBean> getAllsliderimages() {
		List<SliderBean> allsliderimages=new ArrayList<SliderBean>();
		try
		{
			pst=(PreparedStatement) con.prepareStatement(" SELECT id,path,active FROM gallery where type='slider'");
			System.out.println("past "+pst);
			rs=pst.executeQuery();
			while(rs.next()){
				SliderBean img=new SliderBean(); 
				img.setId((rs.getInt(1)));
				img.setPath(rs.getString(2));
				img.setActive(rs.getBoolean(3));
				
				allsliderimages.add(img);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return allsliderimages;
	}

	
	
}

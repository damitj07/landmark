package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Bean.LoinBean;
import Utility.DbUtil;
public class Logindao {

	
	private Connection conn=null;
    private PreparedStatement pst=null;
    private ResultSet rs=null;
	
    public Logindao()
    {
    	conn=DbUtil.getConnection();
    }
	
    public String uservalidation(LoinBean uid)
    {
    	String name = null;
    	try {
    		System.out.println("COnnection is"+conn+"Pst is "+pst);
			pst=conn.prepareStatement("SELECT useremailid FROM USER WHERE useremailid=? and userpassword=?");
			pst.setString(1, uid.getEmailid());
			pst.setString(2, uid.getPassword());
			System.out.println(pst);
			rs=pst.executeQuery();
			while(rs.next())
			{
				name=rs.getString(1);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
		return name;
    }
    public String forgotpassword(String email)
    {	String pass=null;
    	try {
    	PreparedStatement password=conn.prepareStatement("SELECT UserPassword FROM `user` u where UserEmailID=?");
    	password.setString(1, email);
    	ResultSet rsmail=password.executeQuery();
    	while(rsmail.next())
    	{
    		pass=rsmail.getString(1);
    	}
    } catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
    	System.out.println("in logindao"+pass);
		return pass;
    	
    }

	/*public String getRole(LoinBean lg) {
	
		String role = null;
    	try {
			pst=conn.prepareStatement("SELECT userrole FROM USER WHERE useremailid=? and userpassword=?");
			pst.setString(1, lg.getEmailid());
			pst.setString(2, lg.getPassword());
			rs=pst.executeQuery();
			while(rs.next())
			{
				role=rs.getString(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return role;
	}*/
	public String getUserId(LoinBean lg) {
		
	String id="";
    	try {
			pst=conn.prepareStatement("SELECT useremailid FROM USER WHERE useremailid=? and userpassword=?");
			pst.setString(1, lg.getEmailid());
			pst.setString(2, lg.getPassword());
			rs=pst.executeQuery();
			while(rs.next())
			{
				id=rs.getString(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}
}

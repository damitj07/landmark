<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Landmark Design group</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
        <!-- Custom CSS -->
        <link href="css/full-slider.css" rel="stylesheet">

        <style>
            #header {
                min-width: 250px;
                width: 30%;
                background-color: black;
                opacity: 0.8;
                padding: .5em;
                margin-left: .5%;
            }

            #header1 {
                width: 20%;


                display: block;

                left: 0;
                float: right;
                position: fixed;
                top: 0;
                margin-left: 76%;
            }

            #logo .image {
                position: relative;
                margin: 0 0 0 0;
            }

            .nav  a {
                font-size: 1em;
                padding-top: 5%;

            }

            .nav ul li a {
                font-size: 32px;
                padding-top: 5%;
                margin-left: -0.2em;
            }



            @font-face {
                font-family: myFirstFont;
                src: url(fonts/2E9635_0_0.woff);
            }

            a {
                font-family: myFirstFont;
                font-size: 20px;
            }

            h2 {
                font-family: myFirstFont;
                font-size: 26px;
            }

            #header {
                display: block;

                left: 0;
                float: left;
                position:fixed;
                top: 0;
                width: 94%;
            }

            #header .top {
                position: relative;
            }

            #header .bottom {
                border-top: solid 1px rgba(255, 255, 255, 0.05);
                box-shadow: 0 -1px 0 0 rgba(0, 0, 0, 0.15);
                padding-top: 2em;
                margin-top: 1em;
                position: relative;
            }

            .logo1 {
                float: right;

                opacity: 0.8;
            }
        </style>
    </head>

    <body>

        <!-- Navigation -->


        <!-- Full Page Image Background Carousel Header -->
        <header id="myCarousel" class="carousel slide">

            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for Slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <!-- Set the first background image using inline CSS below. -->
                    <div class="fill" style=""><img alt="" src="img/collage- SKF 2.jpg" style="width: 100%;height: 100%"></div>
                    <div class="carousel-caption" >
                        <h2 style="background-color: black;opacity:0.8;width: 50%;margin-left: 50%;">Description Here 1...</h2>
                    </div>
                </div>
                <div class="item">
                    <!-- Set the second background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('');"><img alt="" src="img/collage- SKF 3.jpg" style="width: 100%;height: 100%">></div>
                    <div class="carousel-caption">
                        <h2 style="background-color: black;opacity:0.8;width: 50%;margin-left: 50%;">Description Here 2...</h2>
                    </div>
                </div>
                <div class="item">
                    <!-- Set the third background image using inline CSS below. -->
                    <div class="fill" style="background-image:url('');"><img alt="" src="img/collage- SKF page layout.jpg" style="width: 100%;height: 100%">></div>
                    <div class="carousel-caption">
                        <h2 style="background-color: black;opacity:0.8;width: 50%;margin-left: 50%;">Description Here 3...</h2>
                    </div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="icon-prev"></span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="icon-next"></span>
            </a>

        </header>

        <!-- Page Content -->
        <div id="header" style="background-color: transparent;">
            <div class="row">

                <div class="col-md-4" style="margin-left: 7%">
                    <nav class="navbar navbar-inverse"style="background-color: black;">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle"
                                        data-toggle="collapse" data-target="#myNavbar">
                                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                                        class="icon-bar"></span>
                                </button>

                            </div>
                            <div class="collapse navbar-collapse" id="myNavbar">
                                <div class="nav  id="
                                     myNavbar" style="font-size: 1.5em; padding-top: 5%;">
                                     <!-- <div style="float: right; margin-right: 3%;">
                             <a href="index.html" style="color: rgb(237, 191, 49);">Architecture&nbsp;|</a>&nbsp;<a
                                     href="index.html" style="color: rgb(237, 191, 49)">Interiors&nbsp;|</a>&nbsp;
                             <a href="index.html" style="color: rgb(237, 191, 49);">Sustainability</a>

                                                </div> -->
                                     <div id="logo">
                                        <!-- <span class="image avatar48"><img src="img/logom.png" alt="" /></span>
                        <h1 id="title">Jane Doe</h1>
                        <p>Hyperspace Engineer</p> -->
                                        <img src="img/logoyo.png" alt="" style="width: 99%;">
                                    </div>
                                    <ul class="nav " role="tablist"
                                        style="font-family: 2E9635_0_0; padding-top: 5%">

                                        <li><a href="index.html"
                                               style="color: white; font-family: 2E9635_0_0; font-style: normal;">home
                                                <span class="icon"></span>
                                            </a></li>
                                        <li><a href="displaynews.jsp"
                                               style="color: white; font-family: 2E9635_0_0;">about us </a></li>
                                        <li><a class="dropdown-toggle" data-toggle="dropdown"
                                               href="index.jsp"
                                               style="color: white; font-family: 2E9635_0_0; font-size: 32px; background-color: black; hover: black; border: 0px sloid;">projects
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right" role="menu"
                                                aria-labelledby="menu1"
                                                style=" background-color: black; margin-top: -10%; width: 25%; margin-right: 34%;">
                                                <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                           href="resident.jsp"
                                                                           style="color: white; font-family: 2E9635_0_0;background-color: black; hover: black; ">residential</a></li>
                                                <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                           href="Commercial.jsp"
                                                                           style="color: white; font-family: 2E9635_0_0;background-color: black; hover: black; ">commercial</a></li>
                                                <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                           href="Institutional.jsp"
                                                                           style="color: white; font-family: 2E9635_0_0;background-color: black; hover: black; ">institutional</a></li>
                                                <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                           href="Interiors.jsp"
                                                                           style="color: white; font-family: 2E9635_0_0;background-color: black; hover: black; ">interiors</a></li>
                                            </ul></li>

                                        <li><a href="gallery.jsp"
                                               style="color: white; font-family: 2E9635_0_0;">gallery <span
                                                    class="icon"></span>
                                            </a></li>
                                        <li><a href="downloadfile.jsp"
                                               style="color: white; font-family: 2E9635_0_0;">downloads <span
                                                    class="icon"></span>
                                            </a></li>
                                        <li><a href="contactmap.jsp"
                                               style="color: white; font-family: 2E9635_0_0; float: right;">contact
                                                us <span class="icon"></span>
                                            </a></li>
                                    </ul>
                                </div>
                                <div class="text-center" style="margin-top: 28%;margin-bottom:4%">

                                    <a
                                        style="color: white; font-family: 2E9635_0_0; font-style: normal;">Landmark
                                        2015</a><br /> <a href="http://indigeniusinfoline.com/"
                                                      style="color: white; font-size: 15px; font-family: 2E9635_0_0; font-style: normal;">developed
                                        by Indigenius Infoline Pvt Ltd </a><a
                                        style="color: white; font-size: 15px; font-family: 2E9635_0_0"></a>
                                </div>


                            </div>
                        </div>
                    </nav></div></div>
        </div>    <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Script to Activate the Carousel -->
        <script>
            $('.carousel').carousel({
                interval: 2500 //changes the speed
            })
        </script>

    </body>

</html>

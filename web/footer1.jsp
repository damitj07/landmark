<!DOCTYPE html>
<html lang="en"><head>
  <meta charset="utf-8">
  <title>Landmark Design Group</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  
  <!-- Styles -->
  
  <link rel="stylesheet" href="css/main1.css">



</head>

<body>

    <nav >
    
    <div style="font-family: LuzSans-Book"  >
        <ul class="list-unstyled main-menu" >
     
          <!--Include your navigation here-->
          <li class="text-right" ><a href="#" id="nav-close">X</a></li>
          <li><a href="index.jsp" >home <span class="icon"></span></a></li>
          <li><a href="displaynews.jsp">About us <span class="icon"></span></a></li>
          <li><a href="residential.jsp">Projects <span class="icon"></span></a></li>
                  <!--   <li><a href="#">Dropdown</a>
            <ul class="list-unstyled">
                <li class="sub-nav"><a href="#">Sub Menu One <span class="icon"></span></a></li>
                <li class="sub-nav"><a href="#">Sub Menu Two <span class="icon"></span></a></li>
                <li class="sub-nav"><a href="#">Sub Menu Three <span class="icon"></span></a></li>
                <li class="sub-nav"><a href="#">Sub Menu Four <span class="icon"></span></a></li>
                <li class="sub-nav"><a href="#">Sub Menu Five <span class="icon"></span></a></li>
            </ul>
          </li> -->
           <li><a href="photoGallery.jsp">gallery <span class="icon"></span></a></li>
          <li><a href="downloadfile.jsp">downloads <span class="icon"></span></a></li>
          <li><a href="contactmap.jsp">Contact <span class="icon"></span></a></li>
          <li><a href="adminLogin.jsp">Login <span class="icon"></span></a></li>
        </ul>
        </div>
      </nav>
          
    <div class="navbar navbar-inverse navbar-fixed-top">      
        
        <!--Include your brand here-->
        <a class="navbar-brand" href="index.png"><img src="" class="img-responsive" ></a>
        <div class="navbar-header pull-right">
          <a id="nav-expander" class="nav-expander fixed">
            MENU &nbsp;<i class="fa fa-bars fa-lg white"></i>
          </a>
          
        </div>
        <div></div>
    </div>


     <!-- /container -->
    
    
    <!-- Javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    
   
    
    
    <script>
    
    $(document).ready(function(){												
       
       //Navigation Menu Slider
        $('#nav-expander').on('click',function(e){
      		e.preventDefault();
      		$('body').toggleClass('nav-expanded');
      	});
      	
      	
      	
      	// Initialize navgoco with default options
        $(".main-menu").navgoco({
            caret: '<span class="caret"></span>',
            accordion: false,
            openClass: 'open',
            save: true,
            cookie: {
                name: 'navgoco',
                expires: false,
                path: '/'
            },
            slide: {
                duration: 300,
                easing: 'swing'
            }
        });
  
        	
      });
      </script>


   
    </body>
</html>
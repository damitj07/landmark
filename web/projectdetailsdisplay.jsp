<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>LandMark Design Group</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<style type="text/css">



@font-face {
	font-family: myFirstFont;
	src: url(fonts/LUZRO.TTF);
}

font {
	font-family: myFirstFont;
}

.logo{
	float:left;
	padding: 2em 0em 2em 1em;
	width: 100%;		
	background:black;
	opacity:0.8;
}
</style>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<link href="css/photo/style.css" rel="stylesheet" type="text/css" media="all" />

<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<script src="css/photo/jquery.min.js"></script>
<script src="css/photo/modernizr.custom.js"></script>
<link rel="stylesheet" href="css/photo/swipebox.css">
 <!------ Light Box ------>
    <script src="css/photo/jquery.swipebox.min.js"></script> 
    <script type="text/javascript">
		jQuery(function($) {
			$(".swipebox").swipebox();
		});
	</script>
</head>
<body>


	<div class="container">
<div class="col-md-3">
		<div class="header-top">
			
			<div class="logo" style="height: 650px;margin-left: 10%">
					<a href="index.html"><img src="img/logo.png"class="img-responsive" alt="" /></a> 
					
					<ul class="nav nav-pills nav-stacked" role="tablist">

						<li><a href="index.html" style="color: white;font-family:2E9635_0_0.woff "><h4>home</h4>
								<span class="icon"></span></a></li>
						<li><a href="displaynews.jsp" style="color: white"><h4>about
									us</h4> </a></li>
						<li><a class="dropdown-toggle" data-toggle="dropdown"
							href="index.jsp" style="color: white;"><h4>projects</h4> </a>
							<ul class="dropdown-menu dropdown-menu-right" role="menu"
								aria-labelledby="menu1">
								<li role="presentation"><a role="menuitem" tabindex="-1"
									href="resident.jsp">residential</a></li>
								<li role="presentation"><a role="menuitem" tabindex="-1"
									href="Commercial.jsp">commercial</a></li>
								<li role="presentation"><a role="menuitem" tabindex="-1"
									href="Institutional.jsp">institutional</a></li>
								<li role="presentation"><a role="menuitem" tabindex="-1"
									href="Interiors.jsp">interiors</a></li>
							</ul></li>
						
						<li><a href="photoGallery.jsp" style="color: white"><h4>gallery</h4>
								<span class="icon"></span></a></li>
						<li><a href="downloadfile.jsp" style="color: white"><h4>downloads</h4>
								<span class="icon"></span></a></li>

						<li><a href="contactmap.jsp" style="color: white"><h4 >contact us</h4>
								<span class="icon"></span></a></li>
						
					</ul>
					<div style="margin-left: 40%">
					<a href="index.html" style="color: rgb(237, 191, 49)"><h4>Architecture</h4></a>
					
					<a
						href="index.html"style="color: rgb(237, 191, 49)"><h4>Interiors</h4></a>
					<a href="index.html"style="color: rgb(237, 191, 49)"><h4>Sustainability</h4></a>
					</div>
					
					<a href="#" style="color: white;font-size: 11px;margin-left: 15%">developed by Indigenius
						Infoline Pvt Ltd &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Landmark 2015</a>
			</div>
			<div class="clearfix"> </div>
		</div>
		
	</div>	
		<%
			int cnt1 = 0, cnt2 = 0, cnt3 = 0, cnt4 = 0, cnt5 = 0, cnt6 = 0, cnt7 = 0;
		%>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<c:forEach items="${users}" var="user">
					<%
						if (cnt1 == 0) {
					%>
					<h1>
						<c:out value="${user.projectname}" />
					</h1>
					<%
						cnt1++;
							} else {
							}
					%>
				</c:forEach>
			</div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3 col-md-6" style="background-color: lavender;">
						<c:forEach items="${users}" var="user">
							<%
								if (cnt2 == 0) {
							%>
							<p>
								<c:out value="${user.description}" />
							</p>
							<%
								cnt2++;
									} else {
									}
							%>
						</c:forEach>
					</div>
				</div>
			</div>
			<div class="tab-panel">
				<ul class="tab-link">
					<li class="active"><a href="#FirstTab">Features</a></li>
					<li><a href="#SecondTab">What We did</a></li>
					<li><a href="#ThirdTab">Clients Feedback</a></li>
				</ul>

				<div class="content-area">
					<div id="FirstTab" class="active">

						<c:forEach items="${users}" var="user">

							<%
								if (cnt3 == 0) {
							%>
							<p>
								<c:out value="${user.feature}" />
							</p>
							<%
								cnt3++;
									} else {
									}
							%>
						</c:forEach>
					</div>

					<div id="SecondTab" class="inactive">
						<c:forEach items="${users}" var="user">
							<%
								if (cnt4 == 0) {
							%>
							<p>
								<c:out value="${user.work}" />
							</p>
							<%
								cnt4++;
									} else {
									}
							%>
						</c:forEach>
					</div>

					<div id="ThirdTab" class="inactive">

						<c:forEach items="${users}" var="user">
							<%
								if (cnt5 == 0) {
							%>
							<p>
								<c:out value="${user.feedback}" />
							</p>
							<%
								cnt5++;
									} else {
									}
							%>
						</c:forEach>
					</div>

				</div>
			</div>
		</div>
		<div class="products-section">
		<!-- portfolio-section -->
		
	 <div class="portfolio"  id="portfolio">
	 
				<div id="portfoliolist">
				<c:forEach items="${users}" var="user">
				
				
					<div class="portfolio card mix_all  wow bounceIn" data-wow-delay="0.4s" data-cat="card" style="display: inline-block; opacity: 1;">
						<div class="portfolio-wrapper grid_box">		
							
							<a href="<c:out value="${user.path}" />" class="swipebox"  title="<c:out value="${user.projectname}" />"> <img src="<c:out value="${user.path}" />" class="img-responsive" alt=""><span class="zoom-icon"></span> </a>
							
							
							<h3><c:out value="${user.projectname}" /></h3>
					
		                </div>
								
					
					</div></c:forEach>		
										
					<div class="clearfix"></div>								
				</div>
				
					
    </div>
	  <!-- Script for gallery Here -->
				<script type="text/javascript" src="css/photo/jquery.mixitup.min.js"></script>
					<script type="text/javascript">
					$(function () {
						
						var filterList = {
						
							init: function () {
							
								// MixItUp plugin
								// http://mixitup.io
								$('#portfoliolist').mixitup({
									targetSelector: '.portfolio',
									filterSelector: '.filter',
									effects: ['fade'],
									easing: 'snap',
									// call the hover effect
									onMixEnd: filterList.hoverEffect()
								});				
							
							},
							
							hoverEffect: function () {
							
								// Simple parallax effect
								$('#portfoliolist .portfolio').hover(
									function () {
										$(this).find('.label').stop().animate({bottom: 0}, 200, 'easeOutQuad');
										$(this).find('img').stop().animate({top: -30}, 500, 'easeOutQuad');				
									},
									function () {
										$(this).find('.label').stop().animate({bottom: -40}, 200, 'easeInQuad');
										$(this).find('img').stop().animate({top: 0}, 300, 'easeOutQuad');								
									}		
								);				
							
							}
				
						};
						
						// Run the show!
						filterList.init();
						
						
					});	
					</script>
<!-- portfolio-section  -->
		
		</div>
	</div>
</body>
</html>
<!doctype html>
<html >
    <head>
        <meta charset="UTF-8">
        <title>Landmark Design group</title>
        <meta name="description"content="Design - is a Premium HTML5 Responsive Templeate by Graygrids Team.">
        <meta name="keywords"content="Startup HTML template, Bootstrap Agency theme, html template, charity template, premium html template, one page template">
        <meta name="author" content="GrayGrids">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="assets/css/style.css" >
        <link rel="stylesheet" href="assets/css/responsive.css" >
        <link rel="stylesheet" href="assets/css/normalize.css" >
        <link rel="stylesheet" href="assets/extras/animate.css">
        <link rel="stylesheet" href="assets/extras/lightbox.css">
        <link rel="stylesheet" href="assets/extras/owl/owl.carousel.css">
        <link rel="stylesheet" href="assets/extras/owl/owl.theme.css">
        <link rel="stylesheet" href="assets/extras/owl/owl.transitions.css">
        <!--Standard Includes-->
        <link rel="stylesheet" href="css/style.css"   />
        <link rel="stylesheet" href="css/landmark-helper.css" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"  />
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css"  />
        <link rel="stylesheet" href="css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="http://cdnjs.cloudflare.com/ajax/libs/fancybox/1.3.4/jquery.fancybox-1.3.4.css" />
        <style type="text/css">
            .thumb-style{
                margin-left:10px;
                margin-bottom:10px;
            }
            .img-dimensions{
                width: 200px !important;
                height:130px !important;
            }
        </style>
    </head>
    <body>
        <section id="portfolio" class="bg-container">
            <div class="row">
                <div class="col-md-3 col-xs-3 col-sm-3 col-md-offset-1 col-sm-offset-1 col-xs-offset-1" >
                    <jsp:include page="header.jsp">
                        <jsp:param name="logo" value="img/LDG-logo.png"/>
                        <jsp:param name="subMenu" value="block"/>
                    </jsp:include>
                </div>
                <div   class="portfolio-items wow contentGallery-container fadeInUpQuick col-md-10 col-sm-10 col-xs-10 col-lg-10">
                    <br>
                    <div id="contentGallery" style="width: 500%">
                        <div class="row" id="row1">
                        </div>

                        <div class="row" id="row2">
                        </div>

                        <div class="row" id="row3">
                        </div>
                        <div class="row" id="row4">
                        </div>
                    </div>
                    <br>
                    <div style="text-align: center;margin-left: 32%;" >
                        <a href="#" id="left-button"><i class="fa fa-chevron-left fa-fw fa-2x" style="color: grey;"></i></a>
                        &nbsp;&nbsp;
                        <a href="#" id="right-button"><i class="fa fa-chevron-right fa-fw fa-2x" style="color: grey;"></i></a> 
                    </div>
                </div>
            </div>
        </section>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
            <div class="modal-dialog" role="document" style="left:80px; top:50px;">
                <div class="modal-content">
                    <div class="modal-body">
                        <img id="pop-up-image" style="height: 300px; width: 100%;"/>
                    </div>
                </div>
                <span class="fa fa-close fa-2x" style="float:right;color: white;cursor: pointer;" onclick="closeModal()"></span>
            </div>
        </div>
    </body>
    <script src="assets/js/jquery-1.11.3.min.js"></script>	
    <script src="assets/js/bootstrap.min.js"></script>

    <script>
                    $('#right-button').click(function () {
                        var marg = $('#contentGallery').outerWidth(true) - $('#contentGallery').outerWidth();
                        var noOfImages = $('#contentGallery .row').children().length;
                        var limitOfScroll = (noOfImages - 1) * 80;
                        console.log("martgin : " + marg + "limit of Scroll" + limitOfScroll);
                        if (marg > -limitOfScroll) {
                            $('#contentGallery').animate({
                                marginLeft: "-=280px"
                            }, "fast");
                        }
                    });
                    $('#left-button').click(function () {
                        var marg = $('#contentGallery').outerWidth(true) - $('#contentGallery').outerWidth();
                        if (marg < 280) {
                            $('#contentGallery').animate({
                                marginLeft: "+=280px"
                            }, "fast");
                        }
                    });
                    function callModal(imgPath) {
                        $('#myModal').modal('show');
                        document.getElementById('pop-up-image').src = imgPath;
                    }
                    function closeModal() {
                        $('#myModal').modal('hide');
                    }
                    var row1 = [
                        'img/collage- SKF 4.jpg',
                        'img/collage- SKF page layout.jpg',
                        'img/thumbnail 1.jpg',
                        'img/collage- SKF 4.jpg',
                        'img/thumbnail 1.jpg',
                        'img/thumbnail 1.jpg',
                        'img/collage- SKF 2.jpg',
                        'img/collage- SKF 3.jpg'
                    ];
                    var row2 = [
                        'img/collage- SKF 4.jpg',
                        "img/collage- SKF page layout.jpg",
                        "img/thumbnail 1.jpg",
                        "img/collage- SKF 4.jpg",
                        "img/collage- SKF 4.jpg",
                        "img/thumbnail 1.jpg",
                        "img/thumbnail 1.jpg",
                        "img/thumbnail 1.jpg"
                    ];
                    var row3 = [
                        "img/thumbnail 1.jpg",
                        "img/collage- SKF 4.jpg",
                        "img/collage- SKF 4.jpg",
                        "img/thumbnail 1.jpg",
                        "img/thumbnail 1.jpg",
                        "img/collage- SKF 2.jpg",
                        "img/collage- SKF 3.jpg"
                    ];
                    var row4 = [
                        'img/collage- SKF 4.jpg',
                        "img/collage- SKF page layout.jpg",
                        "img/thumbnail 1.jpg",
                        "img/collage- SKF 4.jpg",
                        "img/collage- SKF 4.jpg",
                        "img/thumbnail 1.jpg",
                        "img/thumbnail 1.jpg",
                        "img/thumbnail 1.jpg"
                    ];
                    for (var i = 0; i < row1.length; i++) {
                        var imgDiv = "<figure class='thumbnails thumb-style' onclick='callModal(\"" + row1[i] + "\")'>" +
                                "<div class='img-dimensions img'>" +
                                "<img src='" + row1[i] + "' alt='' >" +
                                "<div class='overlay'></div>" +
                                "</div></figure>";
                        $('#row1').append(imgDiv);
                    }
                    for (var i = 0; i < row2.length; i++) {
                        var imgDiv = "<figure class='thumbnails thumb-style' onclick='callModal(\"" + row2[i] + "\")'>" +
                                "<div class='img-dimensions img'>" +
                                "<img src='" + row2[i] + "' alt='' >" +
                                "<div class='overlay'></div>" +
                                "</div></figure>";
                        $('#row2').append(imgDiv);
                    }
                    for (var i = 0; i < row3.length; i++) {
                        var imgDiv = "<figure class='thumbnails thumb-style' onclick='callModal(\"" + row3[i] + "\")'>" +
                                "<div class='img-dimensions img'>" +
                                "<img src='" + row3[i] + "' alt='' >" +
                                "<div class='overlay'></div>" +
                                "</div></figure>";
                        $('#row3').append(imgDiv);
                    }
                    for (var i = 0; i < row4.length; i++) {
                        var imgDiv = "<figure class='thumbnails thumb-style' onclick='callModal(\"" + row4[i] + "\")'>" +
                                "<div class='img-dimensions img'>" +
                                "<img src='" + row4[i] + "' alt='' >" +
                                "<div class='overlay'></div>" +
                                "</div></figure>";
                        $('#row4').append(imgDiv);
                    }
    </script>

</html>

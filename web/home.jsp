<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon"
	href="http://static.tmimgcdn.com/img/favicon.ico">
<link rel="icon" href="http://static.tmimgcdn.com/img/favicon.ico">
<title>Landmark Design group</title>
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<!-- Bootstrap Core CSS -->
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- Custom CSS -->
<link href="css/full-slider.css" rel="stylesheet">


<style>
@font-face {
	font-family: myFirstFont;
	src: url(fonts/2E9635_0_0.woff);
}

a {
	font-family: myFirstFont;
	font-size: 20px;
}

h2 {
	font-family: myFirstFont;
	font-size: 26px;
}

.logo1 {
	float: right;
	padding: 2em 0em 2em 1em;
	opacity: 0.8;
}
</style>
</head>
<body>

	<!-- Navigation -->


	<!-- Full Page Image Background Carousel Header -->
	<header id="myCarousel" class="carousel slide">

		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>

		<!-- Wrapper for Slides -->
		<div class="carousel-inner">
			<div class="item active">
				<!-- Set the first background image using inline CSS below. -->
				<div class="fill" style="">
					<img alt="" src="img/Pic 3.jpg" style="width: 100%; height: 100%">
				</div>
				<div class="carousel-caption">
					<h2>PCNTDA</h2>
				</div>
			</div>
			<div class="item">
				<!-- Set the second background image using inline CSS below. -->
				<div class="fill"
					style="background-image: url('fonts\Jellyfish.jpg');">
					<img alt="" src="img/Pic 1.jpg" style="width: 100%; height: 100%">
				</div>
				<div class="carousel-caption">
					<h2>SKF</h2>
				</div>
			</div>
			<div class="item">
				<!-- Set the third background image using inline CSS below. -->
				<div class="fill">
					<img alt="" src="img/Pic 2.jpg" style="width: 100%; height: 100%">
				</div>
				<div class="carousel-caption">
					<h2>SKF</h2>
				</div>
			</div>
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#myCarousel" data-slide="prev">
			<span class="icon-prev"></span>
		</a> <a class="right carousel-control" href="#myCarousel"
			data-slide="next"> <span class="icon-next"></span>
		</a>

	</header>
	<div class="header" >
		<div class="header-top" >
		<div class="logo" >
		<a href="index.html"><img src="img/logom.png"
									class="img-responsive" alt="" style="width: 100%;" /></a>
<ul>
  <li><a href="default.asp" >Home</a></li>
  <li><a href="news.asp">News</a></li>
  <li><a href="contact.asp">Contact</a></li>
  <li><a href="about.asp">About</a></li>
</ul>
</div>
			<div class="container-fluid">
				<div class="row ">
				<div class="col-sm-1 col-md-2 col-lg-1 "></div>
					<div class="col-sm-3 col-md-6 col-lg-4 ">

						<div class="row">
							<div class="logo">
								<a href="index.html"><img src="img/logom.png"
									class="img-responsive" alt="" style="width: 97%;margin-left: 2%" /></a>
<br/>
								<div class="col-sm-2 col-md-4 col-lg-4" style="padding-left: 6%">
									<a href="index.html" style="color: rgb(237, 191, 49)">&nbsp;&nbsp;Architecture</a>
								</div>
								<div class="col-sm-2 col-md-4 col-lg-4">
									<a href="index.html" style="color: rgb(237, 191, 49)">|&nbsp;&nbsp;&nbsp;&nbsp;Interiors&nbsp;&nbsp;&nbsp;&nbsp;|</a>
								</div>
								<div class="col-sm-2 col-md-4 col-lg-4">
									<a href="index.html" style="color: rgb(237, 191, 49);">Sustainability</a>
								</div>
								<br/><br/>
								<ul class="nav nav-pills nav-stacked" role="tablist">


									<li><a href="index.html"
										style="color: white; font-family: 2E9635_0_0; font-style: normal; font-size: 24px;">home
											<span class="icon"></span>
									</a></li>
									<li><a href="displaynews.jsp"
										style="color: white; font-family: 2E9635_0_0; font-size: 24px">about
											us </a></li>
									<li><a class="dropdown-toggle" data-toggle="dropdown"
										href="index.jsp"
										style="color: white; font-family: 2E9635_0_0; font-size: 24px; background-color: black; hover: black; border: 0px sloid; border-color: red">projects
									</a>
										<ul class="dropdown-menu dropdown-menu-right" role="menu"
											aria-labelledby="menu1"
											style="border-top: 1px solid white; border-bottom: 1px solid white;">
											<li role="presentation"><a role="menuitem" tabindex="-1"
												href="resident.jsp"
												style="color: white; font-family: 2E9635_0_0;">residential</a></li>
											<li role="presentation"><a role="menuitem" tabindex="-1"
												href="Commercial.jsp"
												style="color: white; font-family: 2E9635_0_0;">commercial</a></li>
											<li role="presentation"><a role="menuitem" tabindex="-1"
												href="Institutional.jsp"
												style="color: white; font-family: 2E9635_0_0;">institutional</a></li>
											<li role="presentation"><a role="menuitem" tabindex="-1"
												href="Interiors.jsp"
												style="color: white; font-family: 2E9635_0_0;">interiors</a></li>
										</ul></li>

									<li><a href="gallery.jsp"
										style="color: white; font-family: 2E9635_0_0; font-size: 24px">gallery
											<span class="icon"></span>
									</a></li>
									<li><a href="downloadfile.jsp"
										style="color: white; font-family: 2E9635_0_0; font-size: 24px">downloads
											<span class="icon"></span>
									</a></li>
									<br />

									<li><a href="contactmap.jsp"
										style="color: white; font-family: 2E9635_0_0; font-size: 24px; margin-left: 64%">contact
											us <span class="icon"></span>
									</a></li>

								</ul>
<br/>
					<center style="margin-right: 10%"> 
					<a style="color: white;font-size: 15px;font-family: 2E9635_0_0;margin-left: 0%;font-style: normal;">Landmark 2015</a><br/>
						<a href="http://indigeniusinfoline.com/" style="color: white;font-size: 15px;font-family: 2E9635_0_0;margin-left: 0%;font-style: normal;">developed by Indigenius
						Infoline Pvt Ltd </a><a style="color: white;font-size: 15px;font-family: 2E9635_0_0"></a>
						
					
				</center>
							</div>
						</div>
					</div>
					<div class="col-sm-3 col-md-6 col-lg-4"
						style="background-color: lavender;"></div>
					<div class="col-sm-9 col-md-6 col-lg-4"
						style="background-color: lavenderblush;"></div>
				</div>

			</div>

		</div>
	</div>
	<!-- <div class="header">
		<div class="header-top">
			<div class="logo1" style="margin-right:5%"><img src="img/rlogo1.png" class="img-rounded" alt="Cinque Terre" > </div>
			<div class="logo" style="margin-left: 5%">
			
					<a href="index.html"><img src="img/logom.png"class="img-responsive" alt="" style="margin-right: 5px;width: 97%"/></a> 
					<div style="margin-left: 14%;font-size: 16px;">
					<a href="index.html" style="color: rgb(237, 191, 49)">Architecture&nbsp;|</a>&nbsp;<a
						href="index.html"style="color: rgb(237, 191, 49)">Interiors&nbsp;|</a>&nbsp;
					<a href="index.html"style="color: rgb(237, 191, 49);">Sustainability</a>
					</div>
					
					
					<br/><br/>
					<ul class="nav " role="tablist" style="margin-left:-.5em;font-family:2E9635_0_0 ">

						<li><a href="index.html" style="color: white;font-family: 2E9635_0_0 ;font-style: normal;font-size: 24px;">home
								<span class="icon"></span></a></li>	
						<li><a href="displaynews.jsp" style="color: white;font-family: 2E9635_0_0;font-size: 24px">about
									us </a></li>	
						<li><a class="dropdown-toggle" data-toggle="dropdown"
							href="index.jsp" style="color: white;font-family: 2E9635_0_0;font-size: 24px;background-color: black;hover:black;border: 0px sloid;border-color: red">projects </a>	
													<ul class="dropdown-menu dropdown-menu-right" role="menu"
									aria-labelledby="menu1" style="border-top:1px solid white;border-bottom:1px solid white;">
									<li role="presentation"><a role="menuitem" tabindex="-1"
										href="resident.jsp" style="color: white;font-family: 2E9635_0_0;">residential</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1"
										href="Commercial.jsp" style="color: white;font-family: 2E9635_0_0;">commercial</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1"
										href="Institutional.jsp" style="color: white;font-family: 2E9635_0_0;">institutional</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1"
										href="Interiors.jsp" style="color: white;font-family: 2E9635_0_0;">interiors</a></li>
								</ul></li>
							
						<li><a href="gallery.jsp" style="color: white;font-family: 2E9635_0_0;font-size: 24px">gallery
								<span class="icon"></span></a></li>	
						<li><a href="downloadfile.jsp" style="color: white;font-family: 2E9635_0_0;font-size: 24px">downloads
								<span class="icon"></span></a></li>	<br/>

						<li><a href="contactmap.jsp" style="color: white;font-family: 2E9635_0_0;font-size: 24px;margin-left: 64%">contact us
								<span class="icon"></span></a></li>
						
					</ul>	
					<br/>
					<center style="margin-right: 10%"> 
					<a style="color: white;font-size: 15px;font-family: 2E9635_0_0;margin-left: 0%;font-style: normal;">Landmark 2015</a><br/><br/>
						<a href="http://indigeniusinfoline.com/" style="color: white;font-size: 15px;font-family: 2E9635_0_0;margin-left: 0%;font-style: normal;">developed by Indigenius
						Infoline Pvt Ltd </a><a style="color: white;font-size: 15px;font-family: 2E9635_0_0"></a>
						
					
				</center>
			
			</div>	</div>	
			</div>	 -->

	<!-- Page Content -->

	<!-- jQuery -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Script to Activate the Carousel -->
	<script>
		$('.carousel').carousel({
			interval : 4000
		//changes the speed
		})
	</script>

</body>

</html>

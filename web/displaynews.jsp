<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Landmark Design group</title>
        <meta name="description"content="Design - is a Premium HTML5 Responsive Templeate by Graygrids Team.">
        <meta name="keywords"content="Startup HTML template, Bootstrap Agency theme, html template, charity template, premium html template, one page template">
        <meta name="author" content="GrayGrids">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--Standard Includes-->
        <link rel="stylesheet" href="css/style.css"   />
        <link rel="stylesheet" href="css/landmark-helper.css" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"  />
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css"  />
        <link rel="stylesheet" href="css/font-awesome.min.css" />

    </head>
    <body style="height: 100%;">
        <!--<div id="googlemaps" style="height: 100%;width: 100%;position: absolute;"  ></div>-->
            <div class="row">
                <div class="col-md-3 col-xs-3 col-sm-3 col-md-offset-1 col-sm-offset-1 col-xs-offset-1" >
                    <jsp:include page="header.jsp">
                        <jsp:param name="logo" value="img/LM-logo-details.png"/>
                        <jsp:param name="subMenu" value="none"/>
                    </jsp:include>
                </div>
                <div class="col-md-5 col-xs-5 col-sm-5"><h3 style="left-margin:2%">Contents here...</h3></div>
                <div class="col-md-3 col-xs-3 col-sm-3">
                    <div class="landmark-floatings">
                        <div class="container-fluid LB-font-medium">
                            <h3>Latest News</h3>
                            <hr size=1>
                            <marquee behavior="scroll" direction="up" style="margin-left:5%" onmouseover="this.stop();"
                                     onmouseout="this.start();">
                                <p>
                                    Pellentesque habitant morbi tristique senectus et netus et
                                    malesuada fames ac turpis egestas.<br /> <a href="#">4 hours
                                        ago</a>&nbsp;<span>by</span>&nbsp;<a href="">Smith</a>
                                </p>
                                <p>
                                    Pellentesque habitant morbi tristique senectus et netus et
                                    malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat
                                    vitae, ultricies eget,<br /> <a href="#">8 hours ago</a>&nbsp;<span>by</span>&nbsp;<a
                                        href="">Smith</a>
                                </p>
                                <p>
                                    Pellentesque habitant morbi tristique senectus et netus et
                                    malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat
                                    vitae, ultricies eget,<br /> <a href="#">8 hours ago</a>&nbsp;<span>by</span>&nbsp;<a
                                        href="">Smith</a>
                                </p>
                            </marquee>
                            <hr size=1>
                        </div>

                    </div>
                </div>
            </div>
        
        <script src="assets/js/jquery-1.11.3.min.js"></script>	
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="http://maps.googleapis.com/maps/api/js"></script>
        <script type="text/javascript">
            // The latitude and longitude of your business / place
//            var position = [18.65335403424338, 73.75832805424575];
//
//            function initMap() {
//                var myLatLng = {lat: 18.653, lng: 73.758};
//
//                var map = new google.maps.Map(document.getElementById('googlemaps'), {
//                    zoom: 4,
//                    center: myLatLng
//                });
//
//                var marker = new google.maps.Marker({
//                    position: myLatLng,
//                    map: map,
//                    title: 'Hello World!'
//                });
//            }
//            $(document).ready(function(){
//               initMap() ;
//            });
        </script>
    </body>
</html>

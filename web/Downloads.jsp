<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Landmark Design group</title>
        <meta name="description"content="Design - is a Premium HTML5 Responsive Templeate by Graygrids Team.">
        <meta name="keywords"content="Startup HTML template, Bootstrap Agency theme, html template, charity template, premium html template, one page template">
        <meta name="author" content="GrayGrids">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <!--Standard Includes-->
        <link rel="stylesheet" href="css/style.css"   />
        <link rel="stylesheet" href="css/landmark-helper.css" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"  />
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css"  />
        <link rel="stylesheet" href="css/font-awesome.min.css" />
    </head>
    <body>
        <div>
            <div class="row">
                <div class="col-md-3 col-xs-3 col-sm-3 col-md-offset-1 col-sm-offset-1 col-xs-offset-1" >
                    <jsp:include page="header.jsp">
                        <jsp:param name="logo" value="img/LDG-logo.png"/>
                        <jsp:param name="subMenu" value="block"/>
                    </jsp:include>
                </div>

                <div class="table-responsive col-md-8"> 
                    <div><br/><br/><br/></div>    
                    <table  id="filetable"  class="table table-striped" class="table table-bordered" class="table table-striped" class="table table-condensed">
                        <thead>
                            <tr>
                                <th>ImageId</th>
                                <th>Title </th>
                                <th>Description </th>
                                <th>Image</th>

                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${users}" var="user">
                                <tr>					
                                    <td><c:out value="${user.id}" /></td>
                                    <td><c:out value="${user.sub} "/></td>
                                    <td><c:out value="${user.des} "/></td>
                                    <td> <img src="<c:out value="${user.path}" />"width="100" height="100" >	
                                    <td><a href="download?action=download&userid=<c:out value="${user.id}" />">Download</a></td>

                                </tr>
                            </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
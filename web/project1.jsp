<!doctype html>
<html lang="en-US">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html">
  <title>Landmark Design Group</title>
  <meta name="author" content="Jake Rocheleau">
  <link rel="shortcut icon" href="img/sidelogo.jpg">
  <link rel="icon" href="img/sidelogo.jpg">
  <link rel="stylesheet" type="text/css" media="all" href="css/prj.css">
  <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="js/responsiveCarousel.min.js"></script>
   <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

  <style type="text/css">



@font-face {
	font-family: myFirstFont;
	src: url(fonts/2E9635_0_0.woff);
}

a {
	font-family: myFirstFont;
	font-size: 22px;
}

.logo{
	float:left;
	padding: 2em 0em 2em 1em;
	width: 100%;		
	background:black;
	opacity:0.8;
}
.logo .nav li a 
{
	color:white;
	background-color: black;
	
}
.logo .nav li a:HOVER 
{
	color:white;
	background-color:black ; 
}
.logo .nav li ul li a:HOVER 
{
	color:white;
	background-color:black ; 
}
.navbar-nav>li>a {
	padding-top: 12px;
	padding-bottom: 12px;
	
}
.top-nav{
	float:right;
	background:#fff;
	width: 57%;
	padding: 1.5em 4em;
}
.top-nav ul li{
	display: inline-block;
}
</style>
</head>

<body>
<div class="container"> <div class="col-md-3">
<div class="logo" style="height: 650px;margin-left: 10%">
					<a href="index.html"><img src="img/rlogo1.png"class="img-responsive" alt="" /></a> 
					
					<ul class="nav nav-pills nav-stacked" role="tablist">

						<li><a href="index.html" style="color: white; ">home
								<span class="icon"></span></a></li><br/>
						<li><a href="displaynews.jsp" style="color: white">about
									us </a></li><br/>
						<li><a class="dropdown-toggle" data-toggle="dropdown"
							href="" style="color: white;background-color: black;hover:black">projects </a>
							<ul class="dropdown-menu dropdown-menu-right" role="menu"
								aria-labelledby="menu1">
								<li role="presentation"><a role="menuitem" tabindex="-1"
									href="resident.jsp">residential</a></li>
								<li role="presentation"><a role="menuitem" tabindex="-1"
									href="Commercial.jsp">commercial</a></li>
								<li role="presentation"><a role="menuitem" tabindex="-1"
									href="Institutional.jsp">institutional</a></li>
								<li role="presentation"><a role="menuitem" tabindex="-1"
									href="Interiors.jsp">interiors</a></li>
							</ul></li><br/>
						
						<li><a href="gallery.jsp" style="color: white">gallery
								<span class="icon"></span></a></li><br/>
						<li><a href="downloadfile.jsp" style="color: white">downloads
								<span class="icon"></span></a></li>
<br/>
						<li><a href="contactmap.jsp" style="color: white">contact us
								<span class="icon"></span></a></li>
						
					</ul>
					<div style="">
					<a href="index.html" style="font-size: 20px;color: rgb(237, 191, 49);margin-left: 42%">Architectursse |</a>
					
					<a
						href="index.html"style="font-size: 20px;color: rgb(237, 191, 49);margin-left: 53%">Interiors |</a>
					<a href="index.html"style="font-size: 20px;color: rgb(237, 191, 49);margin-left: 37%">Sustainability |</a>
					</div>
					<center>
					<a href="http://indigeniusinfoline.com/" style="color: white;font-size: 12px;">developed by Indigenius
						Infoline Pvt Ltd </a>
						</center>
			</div>
</div>
<div class="col-md-9">
  <div id="w">
    <h1>Landmark</h1>
    
    <nav class="slidernav">
      <div id="navbtns" class="clearfix" >
        <a href="#" class="previous" >prev</a>
        <a href="#" class="next">next</a>
      </div>
    </nav>
    
    <div class="crsl-items" data-navigation="navbtns">
      <div class="crsl-wrap">
        <div class="crsl-item">
          <div class="thumbnail">
            <a href="resident?action=02%20SKF"><img src="img/collage- SKF page layout.jpg" alt="nyc subway"></a>
            <span class="postdate">Feb 16, 2014</span>
          </div>
         <!--  
          <h3><a href="#">Lorem Ipsum Dolor Sit</a></h3>
          
          <p>Suspendisse laoreet eu tortor vel dapibus. Etiam auctor nisl mattis, ornare nibh eu, lobortis leo. Sed congue mi eget velit dictum, id dictum massa tempus. Cras lobortis lectus neque. Fusce aliquet mauris ac bibendum pharetra.</p>
           
          <p class="readmore"><a href="#">Read More &raquo;</a></p>-->
        </div><!-- post #1 -->
        
        <div class="crsl-item">
          <div class="thumbnail" >
          <a href="resident?action=02%20SKF">  <img src="img/collage- SKF 2.jpg" alt="danny antonucci" ></a>
            <span class="postdate">Feb 19, 2014</span>
          </div>
          <!-- 
          <h3><a href="#">A Look Back over A.K.A Cartoon</a></h3>
          
          <p>Vestibulum in venenatis velit. Praesent commodo eget purus sed interdum. Curabitur faucibus purus ut erat fermentum posuere. Suspendisse blandit viverra sagittis. Nullam cursus scelerisque lorem ut ornare.</p>
          
          <p class="readmore"><a href="#">Read More &raquo;</a></p> -->
        </div><!-- post #2 -->
        
        <div class="crsl-item">
          <div class="thumbnail">
            <a href="resident?action=02%20SKF"><img src="img/collage- SKF page layout.jpg" alt="watercolor paints" ></a>
            <span class="postdate">Feb 23, 2014</span>
          </div>
          
          <!-- <h3><a href="#">Watercoloring for Beginners</a></h3>
          
          <p>Cras eget interdum nunc. Nam suscipit congue augue, id interdum risus adipiscing nec. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla non diam id nisi tempus sodales.</p>
          
          <p class="readmore"><a href="#">Read More &raquo;</a></p> -->
        </div><!-- post #3 -->
        
        <div class="crsl-item">
          <div class="thumbnail">
          <a href="resident?action=02%20SKF">  <img src="img/collage- SKF 4.jpg" alt="apple ipod classic photo"></a>
            <span class="postdate">Mar 02, 2014</span>
          </div>
         <!--  
          <h3><a href="#">Classic iPods are Back!</a></h3>
          
          <p>Phasellus condimentum enim nisl, volutpat dapibus turpis euismod nec. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec id elit lorem. Vivamus at eros elit. Nullam cursus euismod purus.</p>
          
          <p class="readmore"><a href="#">Read More &raquo;</a></p> -->
        </div><!-- post #4 -->
        
        <div class="crsl-item">
          <div class="thumbnail">
           <a href="resident?action=02%20SKF"> <img src="img/collage- SKF 3.jpg" alt="web design magazines"></a>
            <span class="postdate">Mar 11, 2014</span>
          </div>
          
          <!-- <h3><a href="#">The 10 Best Web Design Magazines</a></h3>
          
          <p>Morbi quis tempus leo, eget vestibulum quam. Pellentesque ac orci urna. Proin vitae neque vel metus pulvinar luctus vitae eu nulla.</p>
          
          <p class="readmore"><a href="#">Read More &raquo;</a></p> -->
        </div><!-- post #5 -->
      </div><!-- @end .crsl-wrap -->
    </div><!-- @end .crsl-items -->
    
  </div><!-- @end #w -->
  
  </div>
</div>
<script type="text/javascript">
$(function(){
  $('.crsl-items').carousel({
    visible: 2,
    itemMinWidth: 180,
    itemEqualHeight: 370,
    itemMargin: 0,
  });
  
  $("a[href=#]").on('click', function(e) {
    e.preventDefault();
  });
});
</script>
</body>
</html>
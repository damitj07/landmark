<%-- 
    Document   : index
    Created on : 22 Nov, 2015, 12:08:37 AM
    Author     : Amit
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Landmark Design group</title>
        <meta name="description"content="Design - is a Premium HTML5 Responsive Templeate by Graygrids Team.">
        <meta name="keywords"content="Startup HTML template, Bootstrap Agency theme, html template, charity template, premium html template, one page template">
        <meta name="author" content="GrayGrids">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
        <link href="css/landmark-helper.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"  />
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css"  />
        <link rel="stylesheet" href="css/font-awesome.min.css" />

    </head>
    <body>
        <div class="row">
            <!--Side Menu Code Starts Here-->
            <div class="col-md-4 col-xs-4 col-sm-4 col-md-offset-1 col-sm-offset-1 col-xs-offset-1" >
                <jsp:include page="header.jsp">
                    <jsp:param name="logo" value="img/LM-logo-details.png"/>
                    <jsp:param name="subMenu" value="none"/>
                </jsp:include>
            </div>

            <!--Slider Code Starts Here-->
            <div id="LM-home-slider" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="img/Pic 1.jpg" class="img-responsive" >
                    </div>
                    <div class="item">
                        <img src="img/Pic 2.jpg" class="img-responsive" >
                    </div>
                    <div class="item">
                        <img src="img/Pic 3.jpg" class="img-responsive">
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </body>

    <script src="assets/js/jquery-1.11.3.min.js"></script>		
    <script src="assets/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.carousel').carousel({
                interval: 2000
            });
        });
    </script>
    <style type="text/css">
        .submenu{
            color:white !important;
        }
        .submenu:hover{
            color: black !important;
        }
    </style>
</html>

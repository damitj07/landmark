<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Landmark design group</title>

        <meta name="description" content="Design - is a Premium HTML5 Responsive Templeate by Graygrids Team.">
        <meta name="keywords" content="Startup HTML template, Bootstrap Agency theme, html template, charity template, premium html template, one page template">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="GrayGrids">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/header/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/header/css/bootstrap-theme.css" media="screen">
        <link rel='stylesheet' id='camera-css' href='css/header/css/camera.css'type='text/css' media='all'>
        <link rel="stylesheet" href="css/header/css/style.css">
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
        <link href="css/assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" media="screen" href="css/assets/fonts/font-awesome/font-awesome.min.css">
        <link href="css/assets/css/style.css" rel="stylesheet">
        <link href="css/assets/css/responsive.css" rel="stylesheet">
        <link href="css/assets/css/normalize.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/assets/extras/animate.css">
        <link rel="stylesheet" type="text/css" href="css/assets/extras/lightbox.css">
        <link rel="stylesheet" type="text/css" href="css/assets/extras/owl/owl.carousel.css">
        <link href="css/landmark-helper.css" rel="stylesheet" type="text/css"/>
        <script src="js/bootstrap.min.js"></script>


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <section id="portfolio" class="bg-container">
        <div class="container">

            <div class="row">

                <div>
                    <div class="header">
                        <div class="header-top">

                            <div class="logo" style="height: 600px;margin-left: 10%">
                                <a href="index.html"><img src="img/logo.png"class="img-responsive" alt="" /></a> 
                                <div class="row" >
                                    <div class="col-md-1"></div>
                                    <div class="col-md-4"><a href="index.html" style="color: rgb(237, 191, 49)"><h4>Architecture&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</h4></a></div>

                                    <div class="col-md-3"><a
                                            href="index.html"style="color: rgb(237, 191, 49)"><h4>Interiors&nbsp;&nbsp;&nbsp;&nbsp;|</h4></a></div>
                                    <div class="col-md-4"><a href="index.html"style="color: rgb(237, 191, 49)"><h4>Sustainability</h4></a></div>

                                </div>
                                <ul class="nav nav-pills nav-stacked" role="tablist">

                                    <li><a href="index.html" class="LB-font-medium"><h4>home</h4>
                                            <span class="icon"></span></a></li>
                                    <li><a href="displaynews.jsp" style="color: white"><h4>about
                                                us</h4> </a></li>
                                    <li><a class="dropdown-toggle" data-toggle="dropdown"
                                           href="index.jsp" style="color: white;"><h4>projects</h4> </a>
                                        <ul class="dropdown-menu dropdown-menu-right" role="menu"
                                            aria-labelledby="menu1">
                                            <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                       href="resident.jsp">residential</a></li>
                                            <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                       href="Commercial.jsp">commercial</a></li>
                                            <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                       href="Institutional.jsp">institutional</a></li>
                                            <li role="presentation"><a role="menuitem" tabindex="-1"
                                                                       href="Interiors.jsp">interiors</a></li>
                                        </ul></li>

                                    <li><a href="photoGallery.jsp" style="color: white"><h4>gallery</h4>
                                            <span class="icon"></span></a></li>
                                    <li><a href="downloadfile.jsp" style="color: white"><h4>downloads</h4>
                                            <span class="icon"></span></a></li>

                                    <li><a href="contactmap.jsp" style="color: white"><h4 style="margin-left: 70%">contact us</h4>
                                            <span class="icon"></span></a></li>

                                </ul>
                                <a href="#" style="color: white;font-size: 11px;margin-left: 15%">developed by Indigenius
                                    Infoline Pvt Ltd &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Landmark 2015</a>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>				<div >

                    </div></div>	<div class="col-md-5"></div>
                <div class="col-md-7">

                    <div class="controls text-center">
                        <a class="filter btn btn-border" data-filter="all">All</a>
                        <a class="filter btn btn-border" data-filter=".design">Key Projects</a>
                        <a class="filter btn btn-border" data-filter=".marketing">Commercial</a>
                        <a class="filter btn btn-border" data-filter=".nwtwork">Institutional</a>
                        <a class="filter btn btn-border" data-filter=".awesome">Residential</a>
                    </div>
                    <div id="portfolio-items" class="portfolio-items wow fadeInUpQuick">

                        <div class="mix nwtwork" data-myorder="1">
                            <figure>
                                <div class="img">
                                    <img src="img/Key projects/01 PCNTDA/collage PCNTDA 2.jpg" alt="">
                                    <div class="overlay">
                                        <a data-lightbox="image1" href="img/Key projects/01 PCNTDA/collage PCNTDA 2.jpg" class="link-left"><i class="fa fa-plus"></i>

                                        </a>

                                    </div>
                                </div>

                            </figure> 


                        </div>

                        <div class="mix design" data-myorder="2">
                            <figure>
                                <div class="img">
                                    <img src="img/Key projects/01 PCNTDA/collage PCNTDA 1.jpg" alt="">
                                    <div class="overlay">
                                        <a data-lightbox="image1" href="img/Key projects/01 PCNTDA/collage PCNTDA 1.jpg" class="link-left"><i class="fa fa-plus"></i></a>

                                    </div>
                                </div>


                            </figure> 

                            <figure>
                                <div class="img">
                                    <img src="img/Key projects/01 PCNTDA/thumbnail 1.jpg" alt="">
                                    <div class="overlay">
                                        <a data-lightbox="image1" href="img/Key projects/01 PCNTDA/thumbnail 1.jpg" class="link-left"><i class="fa fa-plus"></i></a>
                                        <a href="#" class="link-right"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>

                            </figure>
                        </div>

                        <div class="mix marketing awesome" data-myorder="3">
                            <figure>
                                <div class="img">
                                    <img src="img/Pic 3.jpg" alt="">
                                    <div class="overlay">
                                        <a data-lightbox="image1" href="img/Pic 3.jpg" class="link-left"><i class="fa fa-plus"></i></a>

                                    </div>
                                </div>

                        </div>

                        <div class="mix awesome" data-myorder="4">
                            <figure>
                                <div class="img">
                                    <img src="img/Key projects/01 PCNTDA/collage PCNTDA 4.jpg" alt="">
                                    <div class="overlay">
                                        <a data-lightbox="image1" href="img/Key projects/01 PCNTDA/collage PCNTDA 4.jpg" class="link-left"><i class="fa fa-plus"></i></a>

                                    </div>
                                </div>

                            </figure>          
                        </div>

                        <div class="mix design" data-myorder="5">
                            <figure>
                                <div class="img">
                                    <img src="img/Key projects/01 PCNTDA/collage PCNTDA 4.jpg" alt="">
                                    <div class="overlay">
                                        <a data-lightbox="image1" href="img/Key projects/01 PCNTDA/collage PCNTDA 4.jpg" class="link-left"><i class="fa fa-plus"></i></a>

                                    </div>
                                </div>

                            </figure>    
                            <figure>
                                <div class="img">
                                    <img src="img/Key projects/02 SKF/collage- SKF 1.jpg" alt="">
                                    <div class="overlay">
                                        <a data-lightbox="image1" href="img/Key projects/02 SKF/collage- SKF 1.jpg" class="link-left"><i class="fa fa-plus"></i></a>

                                    </div>
                                </div>

                            </figure>      
                        </div>

                        <div class="mix marketing" data-myorder="6">
                            <figure>
                                <div class="img">
                                    <img src="img/Key projects/01 PCNTDA/collage PCNTDA 4.jpg" alt="">
                                    <div class="overlay">
                                        <a data-lightbox="image1" href="img/Key projects/01 PCNTDA/collage PCNTDA 4.jpg" class="link-left"><i class="fa fa-plus"></i></a>

                                    </div>
                                </div>

                            </figure>  
                            <figure>
                                <div class="img">
                                    <img src="img/Key projects/02 SKF/collage- SKF 2.jpg" alt="">
                                    <div class="overlay">
                                        <a data-lightbox="image1" href="img/Key projects/02 SKF/collage- SKF 2.jpg" class="link-left"><i class="fa fa-plus"></i></a>
                                        <a href="#" class="link-right"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>

                            </figure>          
                        </div>

                        <div class="mix awesome" data-myorder="1">
                            <figure>
                                <div class="img">
                                    <img  src="assets/img/portfolio/img7.jpg" alt="">
                                    <div class="overlay">
                                        <a data-lightbox="image1" href="assets/img/portfolio/thumb/img7.jpg" class="link-left"><i class="fa fa-plus"></i></a>
                                        <a href="#" class="link-right"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>

                            </figure>          
                        </div>  

                        <div class="mix nwtwork design" data-myorder="1">
                            <figure>
                                <div class="img">
                                    <img src="img/Key projects/01 PCNTDA/collage PCNTDA 3.jpg" alt="">
                                    <div class="overlay">
                                        <a data-lightbox="image1" href="img/Key projects/01 PCNTDA/collage PCNTDA 3.jpg" class="link-left"><i class="fa fa-plus"></i></a>
                                        <a href="#" class="link-right"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>

                            </figure>        
                        </div> 

                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <script src="css/assets/js/jquery-min.js"></script>
    <script src="css/assets/js/bootstrap.min.js"></script>
    <script src="css/assets/js/jquery.nav.js"></script>
    <script src="css/assets/js/owl.carousel.js"></script>
    <script src="css/assets/js/wow.js"></script>
    <script src="css/assets/js/jquery.mixitup.js"></script>
    <script src="css/assets/js/lightbox.min.js"></script>
    <script src="css/assets/js/scroll-top.js"></script>
    <script src="css/assets/js/smooth-scroll.js"></script>
    <script src="css/assets/js/style.changer.js"></script>
    <script src="css/assets/js/modernizr-2.8.0.main.js"></script>
    <script src="css/assets/js/main.js"></script>

</body>
</html>
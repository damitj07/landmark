<nav class="navbar navbar-inverse landmark-def-nav container-fluid" >
    <div class="collapse navbar-collapse">
        <div>
            <a href="index.jsp" ><img src="${param.logo}" alt="Landmark Design Group"></a>
        </div>
        <ul class="nav LB-font-bigger" style="background-color: transparent !important;" role="tablist">
            <li><a href="index.jsp" style="padding-left: 0px;">home</a></li>
            <li><a href="displaynews.jsp" style="padding-left: 0px;">about us </a></li>
            <li><a  data-toggle="dropdown"href="index.jsp" style="padding-left: 0px;">projects &nbsp;<span class="caret"></span></a> 
                <ul class="dropdown-menu landmark-def-dropdown" style="left:100px; background-color: transparent !important;">
                    <li ><a href="resident.jsp"  style="color:white !important; " >residential</a></li>
                    <li ><a  href="Commercial.jsp" style="color:white !important;">commercial</a></li>
                    <li ><a  href="Institutional.jsp" style="color:white !important;">institutional</a></li>
                    <li ><a href="Interiors.jsp" style="color:white !important;">interiors</a></li>
                </ul>
            </li>
            <li><a href="gallery.jsp" style="padding-left: 0px;" >gallery </a></li>
            <li><a href="downloadfile.jsp" style="padding-left: 0px;">downloads </a></li>
        </ul>
        <div class="row" style="text-align: right;display: ${param.subMenu}">
            <a href="index.html" class=" LB-font-golden">Architecture&nbsp;</a><br>
            <a href="index.html" class=" LB-font-golden">Interiors&nbsp;</a><br>
            <a href="index.html" class=" LB-font-golden">Sustainability&nbsp;</a>
        </div>
        <br>
        <div class="row LM-nav-ccontacts" >
            <a href="contactmap.jsp" class="LB-font-bigger">contact us </a>
        </div>
        <br>
        <div class="row LM-nav-credits" >
            <a class="LB-font-small">Landmark 2015</a><br /> 
            <a href="http://indigeniusinfoline.com/" class="LB-font-small">developed by Indigenius Infoline Pvt Ltd </a>
        </div>
    </div>
</nav>

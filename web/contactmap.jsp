<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-10">
        <title>Landmark Design group</title>
        <meta name="description"content="Design - is a Premium HTML5 Responsive Templeate by Graygrids Team.">
        <meta name="keywords"content="Startup HTML template, Bootstrap Agency theme, html template, charity template, premium html template, one page template">
        <meta name="author" content="GrayGrids">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="assets/css/style.css" >
        <link rel="stylesheet" href="assets/css/responsive.css" >
        <link rel="stylesheet" href="assets/css/normalize.css" >
        <link rel="stylesheet" href="assets/extras/animate.css">
        <link rel="stylesheet" href="assets/extras/lightbox.css">
        <link rel="stylesheet" href="assets/extras/owl/owl.carousel.css">
        <link rel="stylesheet" href="assets/extras/owl/owl.theme.css">
        <link rel="stylesheet" href="assets/extras/owl/owl.transitions.css">
        <!--Standard Includes-->
        <link rel="stylesheet" href="css/style.css"   />
        <link rel="stylesheet" href="css/landmark-helper.css" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"  />
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css"  />
        <link rel="stylesheet" href="css/font-awesome.min.css" /><meta charset="UTF-10">
        <title>Landmark Design group</title>
        <meta name="description"content="Design - is a Premium HTML5 Responsive Templeate by Graygrids Team.">
        <meta name="keywords"content="Startup HTML template, Bootstrap Agency theme, html template, charity template, premium html template, one page template">
        <meta name="author" content="GrayGrids">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--Standard Includes-->
        <link rel="stylesheet" href="css/style.css"   />
        <link rel="stylesheet" href="css/landmark-helper.css" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css"  />
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css"  />
        <link rel="stylesheet" href="css/font-awesome.min.css" />
    </head>
    <body style="height: 100%;">
       <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0"style="height: 100%;width: 100%;position: absolute;"src="https://maps.google.com/maps?hl=en&q=Landmark Design Group, Pradhikaran&ie=UTF8&t=roadmap&z=12&iwloc=B&output=embed"><div><small>
               <a href="http://embedgooglemaps.com">embedgooglemaps.com</a></small></div><div><small><a href="http://buyproxies.io/">buy proxies</a></small></div></iframe>
        <div class="row" >
            <div class="col-md-4 col-xs-4 col-sm-4 col-md-offset-1 col-sm-offset-1 col-xs-offset-1" >
                <jsp:include page="header.jsp">
                    <jsp:param name="logo" value="img/LM-logo-details.png"/>
                    <jsp:param name="subMenu" value="none"/>
                </jsp:include>
            </div>
            <div class="col-md-3 col-xs-3 col-sm-3 col-sm-offset-4 col-md-offset-4 col-lg-offset-4" style="bottom: 0px;">
                <div class="landmark-floatings">
                    <div class="container-fluid LB-font-medium">
                        <h3>Contact Us</h3>
                        <hr size=1 style="margin:2px;">
                        <address >
                            <strong>Landmark Design Group</strong><br>
                            Plot No.32,Sector No.29,ISCKON <br>
                            Mandir Road, Pradhikaran,Ravet <br>
                            pune-411044
                        </address>
                        <address>
                            Phone : 27653550<br>
                            <a href="mailto:landmarkdg@gmail.com">landmarkdg@gmail.com</a>
                        </address>
                        <h3>Contact Form</h3>
                        <hr size=1 style="margin:2px;">
                        <form method="post" action="Contact?action=insert" class="form-horizontal" style="text-align: left;">
                            <div class="form-group">
                                <label for="name" class="col-sm-4 control-label">Name</label>
                                <input type="text" class="landmark-floatings-from-input" id="name" name="userName" >
                            </div>
                            <div class="form-group">
                                <label for="userPhone" class="col-sm-4 control-label">Contact No.</label>
                                <input type="number" class="landmark-floatings-from-input" id="userPhone" name="userPhone" >
                            </div>
                            <div class="form-group">
                                <label for="userEmail" class="col-sm-4 control-label">Email</label>
                                <input type="email" class="landmark-floatings-from-input" id="userEmail" name="userEmail" >
                            </div>
                            <div class="form-group">
                                <label for="userMsg" class="col-sm-4 control-label">Description</label>
                                <textarea class="landmark-floatings-from-input" rows="2" maxlength="250" id="userMsg" name="userMsg" ></textarea>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default" style="background: none;">Submit</button>
                                </div>
                            </div>
                            <br />
                        </form>
                    </div>
                </div>
            </div>

        </div>

        <script src="assets/js/jquery-1.11.3.min.js"></script>	
        <script src="assets/js/bootstrap.min.js"></script>
    </body>
</html>